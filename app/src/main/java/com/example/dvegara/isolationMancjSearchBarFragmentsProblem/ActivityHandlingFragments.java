package com.example.dvegara.isolationMancjSearchBarFragmentsProblem;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class ActivityHandlingFragments extends AppCompatActivity {

    // <!------------------------------------------ BEGINNING - Defining fragments instances -->
    final HomeFragment fragment_home = new HomeFragment();
    final WorldNewsFragment fragment_world_news = new WorldNewsFragment();
    // <!------------------------------------------ END - Defining fragments instances -->

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handling_fragments);

        final FragmentManager fragment_manager = getSupportFragmentManager();
        FragmentTransaction fragment_transaction = fragment_manager.beginTransaction();  // When app starts
        fragment_transaction.replace(R.id.scrollView, fragment_home);
        fragment_transaction.commit();

        NavigationView navigation_view = (NavigationView) findViewById(R.id.navigation_view);
        navigation_view.getMenu().getItem(0).setCheckable(true);
        navigation_view.getMenu().getItem(0).setChecked(true);

        final DrawerLayout drawer_layout = findViewById(R.id.drawer_layout);
        navigation_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setCheckable(true);
                item.setChecked(true);
                drawer_layout.closeDrawers();

                // <!-------------------------------------- BEGINNING - NavigationView's routing -->
                FragmentTransaction fragment_transaction = fragment_manager.beginTransaction();
                switch(item.getItemId()) {
                    case R.id.menu_item_home_page:
                        fragment_transaction.replace(R.id.scrollView, fragment_home);
                        break;

                    case R.id.menu_item_world_news:
                        fragment_transaction.replace(R.id.scrollView, fragment_world_news);
                        break;
                }
                fragment_transaction.addToBackStack(null);
                fragment_transaction.commit();
                // <!-------------------------------------------- END - NavigationView's routing -->

                return true;
            }
        });
    }


    public HomeFragment getFragmentHome() {
        return fragment_home;
    }

    public WorldNewsFragment getFragmentWorldNews() {
        return fragment_world_news;
    }
}
