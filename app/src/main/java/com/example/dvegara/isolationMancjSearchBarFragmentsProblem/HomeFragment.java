package com.example.dvegara.isolationMancjSearchBarFragmentsProblem;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.SimpleOnSearchActionListener;

import java.lang.reflect.Field;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    private String currency_chosen = "usd";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Sets up the toolbar and the material search bar
     * @param inflater the inflater
     * @param container the container
     * @param savedInstanceState the saved instance state
     * @return the view
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        final AppCompatActivity app_compat_activity = (AppCompatActivity) getActivity();

        setUpMaterialSearchBar(Objects.requireNonNull(app_compat_activity), view);

        return view;
    }

    /**
     * Sets up the material search bar and its events handlers
     * @param app_compat_activity the activity
     * @param view the view
     */
    private void setUpMaterialSearchBar(AppCompatActivity app_compat_activity, View view) {
        final DrawerLayout drawer_layout = app_compat_activity.findViewById(R.id.drawer_layout);
        final MaterialSearchBar material_search_bar = view.findViewById(R.id.material_search_bar);
        material_search_bar.setOnSearchActionListener(new SimpleOnSearchActionListener() {
            @Override
            public void onButtonClicked(int buttonCode) {
                switch (buttonCode){
                    case MaterialSearchBar.BUTTON_NAVIGATION:
                        drawer_layout.openDrawer(Gravity.START);
                        break;
                    case MaterialSearchBar.BUTTON_BACK:
                        material_search_bar.disableSearch();
                        break;
                }
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                super.onSearchConfirmed(text);
                WorldNewsFragment world_news_fragment = new WorldNewsFragment();
                material_search_bar.disableSearch();
                FragmentTransaction fragment_transaction = Objects.requireNonNull(getFragmentManager()).beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putCharSequence("world_news_search_keywords_from_home_fragment", text);
                world_news_fragment.setArguments(bundle);
                fragment_transaction.replace(R.id.scrollView, world_news_fragment);
                fragment_transaction.commit();
            }
        });

        try {  // Setting style for the search bar
            final Field placeHolder = material_search_bar.getClass().getDeclaredField("placeHolder");
            placeHolder.setAccessible(true);
            final TextView textView = (TextView) placeHolder.get(material_search_bar);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
            textView.setPadding(0, 0, 0, 0);
            final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
            layoutParams.setMargins(0,0,0,0);
            Typeface typeface = ResourcesCompat.getFont(Objects.requireNonNull(getContext()), R.font.newscycle_regular);
            textView.setTypeface(typeface);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menu_inflater) {
        menu_inflater.inflate(R.menu.action_bar_menu, menu);
        super.onCreateOptionsMenu(menu,menu_inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // AppCompatActivity app_compat_activity = (AppCompatActivity) getActivity();

        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
