package com.example.dvegara.isolationMancjSearchBarFragmentsProblem;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.SimpleOnSearchActionListener;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class WorldNewsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private DrawerLayout drawer_layout;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public WorldNewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConceptFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WorldNewsFragment newInstance(String param1, String param2) {
        WorldNewsFragment fragment = new WorldNewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        View inflated = inflater.inflate(R.layout.fragment_world_news, container, false);
        final AppCompatActivity activity = (AppCompatActivity) getActivity();

        drawer_layout = activity.findViewById(R.id.drawer_layout);

        setUpMaterialSearchBar(Objects.requireNonNull(activity), inflated);

        return inflated;
    }

    /**
     * Sets up the material search bar and its events handlers
     *
     * @param app_compat_activity the activity
     * @param view                the view
     */
    private void setUpMaterialSearchBar(AppCompatActivity app_compat_activity, final View view) {
        final DrawerLayout drawer_layout = app_compat_activity.findViewById(R.id.drawer_layout);
        final MaterialSearchBar material_search_bar = view.findViewById(R.id.material_search_bar);
        material_search_bar.setOnSearchActionListener(new SimpleOnSearchActionListener() {
            @Override
            public void onButtonClicked(int buttonCode) {
                switch (buttonCode) {
                    case MaterialSearchBar.BUTTON_NAVIGATION:
                        drawer_layout.openDrawer(Gravity.START);
                        break;
                    case MaterialSearchBar.BUTTON_BACK:
                        material_search_bar.disableSearch();
                        break;
                }
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                super.onSearchConfirmed(text);
                getAndDisplayNewsForThisKeywords(getContext(), text);
            }
        });

        //i updated library, and added getter for the placeholder view
        TextView placeHolder = material_search_bar.getPlaceHolder();
        placeHolder.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
        placeHolder.setPadding(0, 0, 0, 0);
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) placeHolder.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, 0);
        Typeface typeface = ResourcesCompat.getFont(Objects.requireNonNull(getContext()), R.font.newscycle_regular);
        placeHolder.setTypeface(typeface);

        if (getArguments() != null && getArguments().getCharSequence("world_news_search_keywords_from_home_fragment") != null) {
            material_search_bar.enableSearch();
            //you can call setText instead of accessing inner views for just changing the search text.
            material_search_bar.setText(getArguments().getCharSequence("world_news_search_keywords_from_home_fragment").toString());
            getAndDisplayNewsForThisKeywords(getContext(), getArguments().getCharSequence("world_news_search_keywords_from_home_fragment"));
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer_layout.openDrawer(GravityCompat.START);
                return true;
        }
        return false;
    }

    private void getAndDisplayNewsForThisKeywords(final Context context, CharSequence keywords) {
        System.out.println("You just have searched for these keywords: " + keywords);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
